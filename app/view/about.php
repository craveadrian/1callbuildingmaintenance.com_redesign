<div id="content">
	<div class="row">
		<h1>ABOUT US</h1>
    <div class="inner-about">
			<p>1 Call Building Maintenance Corp is a full service Cleaning and building Maintenance company serving the Tri State area, we pride ourselves in providing the highest level of service to our clients.</p>

			<p>Our Business stands apart from other commercial cleaning companies because we not only clean your office but maintain it as well. On any given day you could have as many as three separate contractors in your office performing repairs during business hours and this is can be an inconvenience.The reason why it is done that way is because there is no one available after hours to let in and lock up after repair men.</p>

			<p>With 1 Call all you have to do is email or text us your problem e.g (Please repair toilet down the hall water running constantly) and when we come to clean your office that evening we will also do the repairs.</p>

			<p>For major electrical and plumbing problems we have experienced licensed personnel on our staff who will work through the night to have your office up and running the next morning.</p>
    </div>
	</div>
</div>
