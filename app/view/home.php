<div id="content">
	<div id="wlcSection">
		<div class="row">
			<div class="wlcLeft col-7 fl">
				<h3>WELCOME TO</h3>
				<h1>1 CALL <span>BUILDING MAINTENANCE CORP. </span></h1>
				<p>Welcome to 1 Call Building Maintenance. We are a full service Professional Cleaning, Maintenance and Construction Company Serving the New York Tri state Area for the past 20 years. We are commited to providing the highest level of service for the commercial and Health care industry.</p>
				<dl>
					<dt> <a href="<?php echo URL ?>services#content"><img src="public/images/content/wlcImg1.png" alt="Residential Service"> </dt>
					<dd><a href="<?php echo URL ?>services#content">RESIDENTIAL SERVICE</dd>
				</dl>
				<dl>
					<dt> <a href="<?php echo URL ?>services#content"><img src="public/images/content/wlcImg2.png" alt="Commercial Service"> </dt>
					<dd><a href="<?php echo URL ?>services#content">COMMERCIAL SERVICE</dd>
				</dl><br>
				<a href="<?php echo URL ?>about#content" class="btn">READ MORE</a>
			</div>
			<div class="wlcRight col-5 fr">
				<a href="https://youtu.be/R5ri9JHZgWk"><img src="public/images/content/wlcVideo.jpg" alt="Video IMage"></a>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div id="testSection">
		<div class="row">
			<div class="testLeft col-6 fl">
				<img src="public/images/content/testImg1.png" alt="Office">
			</div>
			<div class="testRight col-6 fr">
				<h1>WHAT THEY SAY ABOUT US</h1>
				<img src="public/images/content/profile.png" alt="profile">
				<h4>ELMA V. <span>&#9733;&#9733;&#9733;&#9733;&#9733;</span> </h4>
				<p>“Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ea commodo consequat.”</p>
				<a href="<?php echo URL ?>testimonials#content" class="btn">READ MORE</a>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div id="svcSection">
			<div class="svcLeft col-7 fl">
				<div class="row">
					<h2>CARPET<span>CLEANING</span> </h2>
				</div>
			</div>
			<div class="svcRight col-7 fl">
				<div class="row">
					<h2>FLOOR<span>STRIPPING & WAXING</span> </h2>
				</div>
			</div>
			<div class="clearfix"></div>
	</div>
	<div id="svcSection2">
		<div class="row">
			<div class="svcSection2Right col-4 fr">
				<h2>INTERIOR <span>RENOVATION</span> </h2>
				<p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div id="smSection">
		<div class="row">
			<div class="smPanel fl">
				<p>PROFESSIONAL CLEANING, MAINTENANCE AND CONSTRUCTION</p>
				<p>SERVING NEW YORK TRI STATE AREA FOR THE PAST 20 YEARS.</p>
				<p>FOLLOW US:</p>
				<p>
					<a href="<?php $this->info("fb_link"); ?>" target="_blank"> <img src="public/images/common/fb.png" alt="facebook"> </a>
					<a href="<?php $this->info("tt_link"); ?>" target="_blank"> <img src="public/images/common/tt.png" alt="twitter"> </a>
					<a href="<?php $this->info("gp_link"); ?>" target="_blank"> <img src="public/images/common/gp.png" alt="google plus"> </a>
					<a href="<?php $this->info("ln_link"); ?>" target="_blank"> <img src="public/images/common/ln.png" alt="link"> </a>
					<a href="<?php $this->info("ig_link"); ?>" target="_blank"> <img src="public/images/common/ig.png" alt="instagram"> </a>
				</p>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div id="gallSection">
		<div class="row">
			<div class="images">
				<img src="public/images/content/gall1.jpg" alt="Gallery 1" class="col-sm fl">
				<img src="public/images/content/gall2.jpg" alt="Gallery 2" class="col-sm fl">
				<img src="public/images/content/gall3.jpg" alt="Gallery 3" class="col-sm fl">
				<img src="public/images/content/gall4.jpg" alt="Gallery 4" class="col-sm fl">
				<img src="public/images/content/gall5.jpg" alt="Gallery 5" class="col-sm fl">
				<div class="clearfix"></div>
			</div>
			<a href="<?php echo URL ?>gallery#content" class="btn">READ MORE</a>
		</div>
	</div>
	<div id="mapSection">
		<div class="row">
			<div class="map">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3025.699206420386!2d-73.96515678459598!3d40.68059607933532!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c25ba3078137bd%3A0x6a3fd0aae5ed71cd!2s946+Atlantic+Ave%2C+Brooklyn%2C+NY+11238%2C+USA!5e0!3m2!1sen!2sph!4v1533856644149" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</div>
	</div>
	<div id="cntSection">
		<div class="row">
			<div class="cntLeft col-5 fl">
				<a href="<?php echo URL ?>"> <img src="public/images/common/footLogo.png" alt="1 Call Building Main Logo"> </a>
				<p class="socialMedia">
					<a href="<?php $this->info("fb_link"); ?>" class="socialico" target="_blank">F</a>
					<a href="<?php $this->info("tt_link"); ?>" class="socialico" target="_blank">L</a>
					<a href="<?php $this->info("yt_link"); ?>" class="socialico" target="_blank">X</a>
					<a href="<?php $this->info("rss_link"); ?>" class="socialico" target="_blank">R</a>
				</p>
			</div>
			<div class="cntMid col-2 fl">
				<h4>CONTACT INFO</h4>
				<p> <img src="public/images/common/cntLocation.png" alt="location logo"> <span>946 Atlantic Ave <br>Brooklyn, NY 11238 </span> </p>
				<p> <img src="public/images/common/cntPhone.png" alt="Phone logo"> <span><?php $this->info(["phone","tel"]); ?> </span> </p>
				<p> <img src="public/images/common/cntMail.png" alt="Email logo"> <span><?php $this->info(["email","mailto"]); ?> </span> </p>
			</div>
			<div class="cntRight col-5 fl">
				<h4>GET IN TOUCH</h4>
				<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
					<label class="col-6 fl"><span class="ctc-hide">Name</span>
						<input type="text" name="name" placeholder="Name:">
					</label>
					<label class="col-6 fl"><span class="ctc-hide">Email</span>
						<input type="text" name="email" placeholder="Email:">
					</label>
					<div class="clearfix"></div>
					<label><span class="ctc-hide">Message</span>
						<textarea name="message" cols="30" rows="10" placeholder="Message:"></textarea>
					</label>
					<label for="g-recaptcha-response"><span class="ctc-hide">Recaptcha</span></label>
					<div class="g-recaptcha"></div>
					<label>
						<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
					</label><br>
					<?php if( $this->siteInfo['policy_link'] ): ?>
					<label>
						<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
					</label>
					<?php endif ?>
					<button type="submit" class="ctcBtn btn" disabled>SUBMIT</button>
				</form>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
